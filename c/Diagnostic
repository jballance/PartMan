/* Copyright 2016 Castle Technology Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// c.diagnostic
//
//  various diagnostic routines

#include        "PMInclude.h"
#include        "FormatDef.h"
#include        "Diagnostic.h"


#if Debug

//#define debugfile "ram:$.TestFile"
#define debugfile "RAM:$.TestFile"
#define datafile  "RAM:$.DataFile"

// init testfile to zero length
void testfileinit (void)
{
  FILE * fp;
  if((fp = fopen( debugfile,"w")) != NULL)
  {
    fclose(fp);
  }
  if((fp = fopen( datafile,"w")) != NULL)
  {
    fclose(fp);
  }
}
// save item to disc appended to end of testfile
void testsave (char* title, char * data, int size)
{
  FILE * fp;
  if((fp = fopen( datafile,"ab")) != NULL)
  {
    fprintf(fp,"\n\n%s:\n\n",title);
    fwrite(data,size,1,fp);
    fclose(fp);
  }
}
// print item to disc appended to end of testfile
void testprint (char * fmt, ...)
{
  FILE * fp;
  va_list arg;
  va_start (arg,fmt);
  _kernel_swi_regs rr;
  if((fp = fopen( debugfile,"ab")) != NULL)
  {
    _kernel_swi(OS_ReadMonotonicTime,&rr,&rr);
    fprintf(fp," %d: ",rr.r[0]);
    vfprintf(fp,fmt,arg);
    fclose(fp);
  }
  va_end(arg);
}
void testprintGUID (char * ptr)
{
  testprint(" %02x%02x%02x%02x-",ptr[3],ptr[2],ptr[1],ptr[0]);
  testprint("%02x%02x-",ptr[5],ptr[4]);
  testprint("%02x%02x-",ptr[7],ptr[6]);
  testprint("%02x%02x-",ptr[8],ptr[9]);
  testprint("%02x%02x%02x",ptr[10],ptr[11],ptr[12]);
  testprint("%02x%02x%02x ",ptr[13],ptr[14],ptr[15]);
}
void reportfmt(FormatArray* fmt)
{

  testprint("\nfmt->log2secsize %x",fmt->log2secsize);
  testprint("\nfmt->idlen       %x",fmt->idlen);
  testprint("\nfmt->log2bpmb    %x",fmt->log2bpmb );
  testprint("\n     (LFAU)      %x",1<<fmt->log2bpmb );
  testprint("\nfmt->zonecount   %x",fmt->zonecount);
  testprint("\nfmt->zonecount2  %x",fmt->zonecount2);
  testprint("\nfmt->zonetotal  &%x %d",fmt->zonec,fmt->zonec);
  testprint("\nfmt->bigdisc     %x",fmt->bigdisc);
  testprint("\nfmt->bigdir      %x",fmt->bigdir);
  testprint("\nfmt->zonespare   %x",fmt->zonespare);
  testprint("\nfmt->SecSize     %x",fmt->SecSize);
  testprint("\nfmt->SecCount    %x",fmt->SecCount);
  testprint("\nfmt->endzonebits %x",fmt->endzonebits);
  testprint("\nfmt->rootaddress %x",fmt->rootaddress);
  testprint("\nfmt->rootdiradd  %x",fmt->rootdiradd);
  testprint("\nfmt->newrootaddr %x",fmt->newrootaddress);
  testprint("\nfmt->mapbase     %x",fmt->mapbase);
  testprint("\nfmt->discsize    %x",fmt->discsize);
  testprint("\nfmt->discsizehi  %x",fmt->discsizehi);
  testprint("\nfmt->log2zsize   %x",fmt->log2zsize);
  testprint("\nfmt->newshsize   %x",fmt->newsharesize);
  testprint("\nfmt->mapbits     %x",fmt->mapbits);
  testprint("\nfmt->dirbits     %x",fmt->dirbits);
  testprint("\nfmt->bootsecbits %x",fmt->bootsecbits);
  testprint("\nfmt->fragcount   %x",fmt->fragmentcount);
  testprint("\nfmt->fragmissed  %x",fmt->fragmentmissed);
  testprint("\nfmt->fragsZ0     %x",fmt->fragmentzone0);
  testprint("\nfmt->fragsZnorm  %x",fmt->fragmentzonenorm);
  testprint("\nfmt->fragsZlast  %x",fmt->fragmentzonelast);
  testprint("\nfmt->z0sparefnd  %x",fmt->zone0sparefound);
  testprint("\nfmt->zsparefnd   %x",fmt->zonesparefound);
  testprint("\nfmt->cyls        %d",fmt->cyls);
  testprint("\nfmt->spt         %d",fmt->spt);
  testprint("\nfmt->heads       %d",fmt->heads);
  testprint("\nfmt->density     %x",fmt->density);
  testprint("\nfmt->skew        %x",fmt->skew);
  testprint("\n");
}
void reportAcornBootSec(disc_record* dr)
{
  testprint("\nlog2secsize %x",dr->l2secsize);
  testprint("\nHeads      %3d\nspt         %2d",dr->heads,dr->spt);
  testprint("\ndensity     %d",dr->density);
  testprint("\nidlen       %d",dr->idlen);
  testprint("\nl2bpmb      %d",dr->l2bpmb);
  testprint("\nskew        %d",dr->skew);
  testprint("\nbootoption  %d",dr->bootoption);
  testprint("\nlowsector   %x",dr->lowsector);
  testprint("\nnzones      %04x",dr->nzones+(dr->nzones2<<8));
  testprint("\nzonespare   %x",dr->zonespare[0]+(dr->zonespare[1]<<8));
  testprint("\nroot        %x",dr->root);
  testprint("\ndiscsize    %08x%08x",dr->discsizehi,dr->discsize);
  testprint("\ndiscid      %04x",dr->discid[0]+(dr->discid[1]<<8));
  testprint("\ndiscname    %10s",dr->discname);
  testprint("\nlog2zsize   %d",dr->log2zsize);
  testprint("\nbigflag     %d",dr->bigflag);
  testprint("\nformatversn %x",dr->formatversion);
  testprint("\nrootdirsize %x\n",dr->rootdirsize);
}


#else

#define _UU(x) ((void)(x))

void testfileinit (void){}
void testsave (char* title, char * data, int size){_UU(title);_UU(data);_UU(size); }
void testprint (char * data, ...){_UU(data);}
void reportfmt(FormatArray* fmt){_UU(fmt);}
void testprintGUID (char * ptr) {_UU(ptr);}
void reportAcornBootSec(disc_record* dr){_UU(dr);}




#endif

